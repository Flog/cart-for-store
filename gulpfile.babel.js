const gulp = require('gulp');
const babel = require('gulp-babel');
const cssnano = require('gulp-cssnano');
const uglify = require('gulp-uglify');
const autoprefixer = require('gulp-autoprefixer');
const imagemin = require('gulp-imagemin');
const cache = require('gulp-cache');
const sourcemaps = require('gulp-sourcemaps');
const browserSync = require('browser-sync').create();
const del = require('del');
const htmlmin = require('gulp-htmlmin');

const paths = {
    html:{
        src: 'app/*.html',
        dest: 'dist/'
    },
    styles: {
        src: 'app/**/*.css',
        dest: 'dist/'
    },
    scripts: {
        src: 'app/**/*.js',
        dest: 'dist/'
    },
    images: {
        src: 'app/img/**/*.+(png|jpg|jpeg|gif|svg)',
        dest: 'dist/img'
    }
};
function clean() {
    return del([ 'dist' ]);
}
function html() {
    return gulp.src(paths.html.src)
        .pipe(htmlmin({ collapseWhitespace: true }))
        .pipe(gulp.dest(paths.html.dest));
}
function styles() {
    return gulp.src(paths.styles.src, { sourcemaps: true })
        .pipe(sourcemaps.init())
        .pipe(cssnano())
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(paths.styles.dest));
}
function scripts() {
    return gulp.src(paths.scripts.src, { sourcemaps: true })
        .pipe(babel({
            presets: ['@babel/env']
        }))
        .pipe(uglify())
        .pipe(gulp.dest(paths.scripts.dest));
}
function images() {
    return gulp.src(paths.images.src)
        .pipe(cache(imagemin({
            interlaced: true
        })))
        .pipe(gulp.dest(paths.images.dest));
}
function watchFiles() {
    gulp.watch(paths.scripts.src, scripts);
    gulp.watch(paths.styles.src, styles);
}
function browsersync() {
    browserSync.init({
        server: {
            baseDir: "app"
        }
    });
    browserSync.watch("app", browserSync.reload);
}

const build = gulp.series(clean, html, gulp.parallel(styles, scripts));
const watch = gulp.parallel(watchFiles, browsersync);
exports.clean = clean;
exports.html = html;
exports.styles = styles;
exports.scripts = scripts;
exports.watch = watch;
exports.build = build;
exports.default = build;