var productList = [
    {name: 'Video Suite', box: 'img/vs.png', price: 149, desc: 'Some description here', id: 'vs'},
    {name: 'Bundle Photo Editor', box: 'img/bundlepe.png', price: 100, desc: 'Some description here', id: 'bpe'},
    {name: 'Photo Editor', box: 'img/pe.png', price: 132, desc: 'Some description here', id: 'pe'},
    {name: 'Photo Studio', box: 'img/ps.png', price: 87, desc: 'Some description here', id: 'ps'},
    {name: 'Screen Capture', box: 'img/sc.png', price: 151, desc: 'Some description here', id: 'sc'}
];

var cart = {
    wrapper: '',
    addedItemsIds: [],
    add: function (id) {
        if (!this.has(id)) {
            this.addedItemsIds.push(id);
            this.save();
        }
        this.render();
    },
    save: function () {
        localStorage.setItem('addedItemsIds', JSON.stringify(this.addedItemsIds));
    },
    load: function () {
        var data = localStorage.getItem('addedItemsIds');
        if (data != null) {
            this.addedItemsIds = JSON.parse(data);
        }
    },
    has: function (id) {
        for (let item of this.addedItemsIds) {
            if (id === item) {
                return true;
            }
        }
        return false;
    },
    getProductById: function (id) {
        for (let product of productList) {
            if (id === product.id) {
                return product;
            }
        }
    },
    render: function () {
        let cart = this;
        this.wrapper.empty();
        for (let productId of this.addedItemsIds) {
            let product = cart.getProductById(productId);
            let itemTemplate = $(`<div class="list-item-row">
                                <img src="img/cross.png" alt="" class="cross-icon">
                                <img src="${product.box}" alt="" width="45" class="box-img">
                                <span>${product.name}</span>
                                <span>$${product.price}</span>
                            </div>`);
            cart.wrapper.append(itemTemplate);
            itemTemplate.find('.cross-icon').on('click', function () {
                cart.remove(productId, renderHtml);
            });
        }
        var totalBlock = $(`<div class="cart-total">
                                <p>
                                    <span>Total</span>
                                    <span class="sum-order">$${cart.total()}</span>
                                </p>
                            </div>`);
        cart.wrapper.append(totalBlock);

    },
    remove: function (id, callback) {
        for (let i = 0; i < this.addedItemsIds.length; i++) {
            if (id === this.addedItemsIds[i]) {
                this.addedItemsIds.splice(i, 1);
            }
        }
        this.save();

        if (callback !== undefined) {
            callback();
        }
        this.render();
    },
    total: function () {
        let cart = this;
        let total = 0;
        this.addedItemsIds.forEach(function (id) {
            let item = cart.getProductById(id);
            total += item.price;
        });

        return total;
    },
    clear: function () {
        this.addedItemsIds.length = 0;
        this.save();
        this.render();
    },
    addAll: function () {
        for(let product of productList){
            this.add(product.id);
        }
        this.save();
        this.render();
    }
};

var renderHtml = function () {
    var itemsWrapper = $('.mjs-items-wrapper');
    itemsWrapper.empty();

    for (let product of productList) {
        var disabledBtn = cart.has(product.id) ? 'disabled' : '';
        var template = $(`<div class="item-block">
                             <img src="${product.box}" alt="" class="item-img">
                             <div>
                                 <h3 class="item-title">${product.name}</h3>
                                 <p class="item-desc">${product.desc}</p>
                             </div>
                             <div class="item-price-block">
                                <p class="item-price">$${product.price}</p>
                                <button class="item-price-button ${disabledBtn}" data-id="${product.id}">Add to Cart</button>
                             </div>
                        </div>`);
        itemsWrapper.append(template);
        template.find($('.item-price-button')).on('click', function () {
            var id = $(this).attr('data-id');
            cart.add(id);
            renderHtml(productList);
        })
    }

};
$(function () {
    cart.load();

    renderHtml();
    cart.wrapper = $('.cart-wrapper .mjs-cart-list-items');
    cart.render();

    $('#clear-cart').on('click', function () {
        cart.clear();
        renderHtml(productList);
    });
    $('#add-all').on('click', function () {
        cart.addAll();
        renderHtml(productList);
    });
});